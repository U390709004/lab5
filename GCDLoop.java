public class GCDLoop {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int k = 1;
        while (k != 0) {
            k = a % b;
            if (k != 0) {
                a = b;
                b = k;

            }

        }
        System.out.println(b);

    }
}
