public class GCDRec {
    public static void main(String[] args) {
        int a = Integer.parseInt(args[0]);
        int b = Integer.parseInt(args[1]);
        int k = 1;
        while (k != 0) {
            k = a % b;
            if (k != 0) {
                a = b;
                b = k;

            }

        }
        System.out.println(findGCD(a,b));

    }

    private static int findGCD(int number1, int number2) {

        if(number2 == 0){
            return number1;
        }
        return findGCD(number2, number1%number2);
    }
}
